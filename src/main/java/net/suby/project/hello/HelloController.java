package net.suby.project.hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ohs on 2017-01-10.
 */
@RequestMapping(value = "/hello")
@Controller
public class HelloController {

    @RequestMapping(value = "/helloWorld", method = RequestMethod.GET)
    public String hello(){
        return "hello/helloWorld";
    }
}
